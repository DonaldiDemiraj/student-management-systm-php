<?php
session_start();
    if(isset($_SESSION['uid']))
    {
        echo "";
    }
    else
    {
       header('location: ../login.php');
    }

?>
<?php
include('header.php');
include('titlehead.php');
include('../dbcon.php');

$sid  = $_GET['sid'];

$sql = "SELECT * FROM `student` WHERE id ='$sid'";
$run = mysqli_query($con,$sql);

$data = mysqli_fetch_assoc($run);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>
    <body>

        <form action="updateform.php" method="post" enctype="multipart/form-data">
            <table align="center" border="1" style="width:70%; margin-top:40px;">
                <tr>
                    <th>Roll No</th>
                    <td><input type="text" name="rollno" value="<?php echo $data['rollno']; ?>" required></td>
                </tr>
                <tr>
                    <th>Full Name</th>
                    <td><input type="text" name="name"  value="<?php echo $data['name']; ?>" required></td>
                </tr>
                <tr>
                    <th>City</th>
                    <td><input type="text" name="city"  value="<?php echo $data['city']; ?>" required></td>
                </tr>
                <tr>
                    <th>Paretns Contact No</th>
                    <td><input type="text" name="pcont"  value="<?php echo $data['pcont']; ?>" required></td>
                </tr>
                <tr>
                    <th>Stander</th>
                    <td><input type="number" name="stander"  value="<?php echo $data['stander']; ?>" required></td>
                </tr>
                <tr>
                    <th>Image</th>
                    <td><input type="file" name="simg" required></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="hidden" name="sid" value="<?php echo $data['id']; ?>">
                        <input type="submit" name="submit" class="btn btn-success" value="Update" >
                    </td>
                </tr>
            </table>
        </form>

    </body>
</html>

