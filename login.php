<?php

session_start();
if(isset($_SESSION['uid']))
{
    header('location: admin/admindash.php');
}

require_once 'dbcon.php';

if(isset($_POST['login']))
{
    $username = $_POST['uname'];
    $pwd = $_POST['psw'];
    $password = MD5($pwd);

    $query = "SELECT * FROM admin WHERE username = '$username' AND password = '$pwd'";
    $run = mysqli_query($con,$query);
    $row = mysqli_num_rows($run);
    if($row < 1)
    {
        ?>
        <script>
            alert('Username or Password not match !!');
            window.open('login.php','_self');
        </script>
        <?php
    }
    else
    {
        $data = mysqli_fetch_assoc($run);
        $id = $data['id'];

        
        $_SESSION['uid'] = $id;
        header('location: admin/admindash.php');
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <br><br>
    <h1 align="center">Admin Login</h1>
    <form action="login.php" method="post">
        <div class="imgcontainer">
            <img src="https://f0.pngfuel.com/png/81/570/profile-logo-png-clip-art.png" style="height:80px;" alt="Avatar" class="avatar">
        </div>

        <div class="container" style="margin-left:500px;">
            <div class="row">
                <div class="col-sm-4" align="center">
                    <label for="uname"><b>Username</b></label>
                    <input type="text" placeholder="Enter Username" name="uname" required><br><br>

                    <label for="psw"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="psw" required><br><br>

                    <button class="btn btn-success form-control" type="login" name="login">Login</button>
                </div>
            </div>
        </div>
        <br>

        <div class="container" style="background-color:#f1f1f1; margin-left:700px;">
            <span class="psw">Forgot <a href="#">password?</a></span>
        </div>
    </form>
</body>
</html>